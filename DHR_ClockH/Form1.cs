﻿using DHR_ClockH.DHRDataWarehouseDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DHR_ClockH
{
    public partial class Form1 : Form
    {
        public List<string> ColumnNames;

        DHRDataWarehouseDataSet.ClockH_MasterRow CurrentMasterRow;
        //DHRDataWarehouseDataSet.ClockH_SettingsRow CurrentSettingsRows;

        DHRDataWarehouseDataSetTableAdapters.ClockH_MasterTableAdapter currentMasterTableAdapter;
        DHRDataWarehouseDataSetTableAdapters.ClockH_SettingsTableAdapter currentSettingsTableAdapter;

        String FileName;


        public Form1()
        {
            InitializeComponent();

            this.currentSettingsTableAdapter = new ClockH_SettingsTableAdapter();


            //get the types and populate the combobox of the types column of the grid
            var table = (DHRDataWarehouseDataSet.ClockH_ColumnTypesDataTable)this.ColumnTypesTable_Get();
            DataGridViewComboBoxColumn typeColumn = (DataGridViewComboBoxColumn)dgViewMap.Columns["ColumnType"];
            var l = table.ToList();
            typeColumn.DataSource = table.ToList();
            
            typeColumn.DisplayMember = table.DescriptionColumn.ColumnName;
            typeColumn.ValueMember = table.IdColumn.ColumnName;

            
            
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            SavedConfigs_Load();

            cmbConfigName.SelectedIndex = -1;

            //vals.Insert(0, "");

            CurrentMasterRow_Get("");

            this.cmbConfigName.SelectedIndexChanged += new System.EventHandler(this.cmbConfigName_SelectedIndexChanged);

            this.clockH_MasterTableAdapter.Fill(this.dHRDataWarehouseDataSet.ClockH_Master);
            this.currentSettingsTableAdapter.Fill(this.dHRDataWarehouseDataSet.ClockH_Settings);

            //this.dgViewMap.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgViewMap_RowsAdded);
            this.dHRDataWarehouseDataSet.ClockH_Settings.TableNewRow += ClockH_Settings_TableNewRow;

            //CurrentMasterRow_Get();

            
        }

        void SavedConfigs_Load()
        {
            //Get the saved config names
            var vals = (from r in MasterTable_Get()
                        select r.ConfigName.ToString()).ToList();

            this.cmbConfigName.DataSource = vals.ToList();
        }

        //get the Available Configs
        private DHRDataWarehouseDataSet.ClockH_MasterDataTable MasterTable_Get()
        {
            var tb = new DHRDataWarehouseDataSet.ClockH_MasterDataTable();
            this.currentMasterTableAdapter = new ClockH_MasterTableAdapter();
            this.currentMasterTableAdapter.Fill(tb);

            return tb;
        }

        //get the Available Configs
        private void CurrentMasterRow_Get(string ConfigName)
        {
            if (ConfigName == null || ConfigName == "")
             {
                this.clockHSettingsBindingSource.Filter = "ConfigName = null ";
                this.txtClientId.Text = string.Empty;
                return;
            }
                
            //this.CurrentMasterTable = new DHRDataWarehouseDataSet.ClockH_MasterDataTable();
            var tb = MasterTable_Get();
            this.CurrentMasterRow = tb.Where(r => r.ConfigName == ConfigName).First();
            
            //Udpate the controls
            this.cmbFormat.Text = this.CurrentMasterRow.Format;
            this.txtClientId.Text = this.CurrentMasterRow.ClientID;
            this.chkFileHasHeader.Checked = this.CurrentMasterRow.HasHeader ;

            this.clockHSettingsBindingSource.Filter = string.Format("ConfigName = '{0}'", ConfigName);
        }

 
        // Gets the availabe Comumn types
        private DataTable ColumnTypesTable_Get()
        {
            var tb = new DHRDataWarehouseDataSet.ClockH_ColumnTypesDataTable();
            var ta = new ClockH_ColumnTypesTableAdapter();
            ta.Fill(tb);
            return tb;
        }

        private void btSelectFileToMap_Click(object sender, EventArgs e)
        {
            if (this.ofdSelectFileToMap.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this.FileName = ofdSelectFileToMap.FileName;
                    this.lblStatus.Text = "Selected file: " + Path.GetFileName(this.FileName);
                    this.lblStatus.ToolTipText = this.FileName;

                    //this.ColumnNames = new csvReader().GetColumnNamesFromCsv(fileName);
                    //DataGridViewComboBoxColumn d = (DataGridViewComboBoxColumn )dgViewMap.Columns["ColumnName"];
                    //d.DataSource = this.ColumnNames;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk: " + ex.Message);
                }
            }
        }

        private bool HasDataChanged()
        {
            if ((MasterRow_GetChanges() != null) ||
                (this.dHRDataWarehouseDataSet.ClockH_Settings.GetChanges() != null))
                return true;

            return false;
        }

        private DHRDataWarehouseDataSet.ClockH_MasterDataTable MasterRow_GetChanges(){

            if (this.CurrentMasterRow == null) return null;

            if (this.CurrentMasterRow.ClientID != this.txtClientId.Text) this.CurrentMasterRow.ClientID = this.txtClientId.Text;
            if (this.CurrentMasterRow.Format != this.cmbFormat.Text) this.CurrentMasterRow.Format = this.cmbFormat.Text;
            if (this.CurrentMasterRow.HasHeader != this.chkFileHasHeader.Checked) this.CurrentMasterRow.HasHeader = this.chkFileHasHeader.Checked;

            return (DHRDataWarehouseDataSet.ClockH_MasterDataTable)this.CurrentMasterRow.Table.GetChanges();
        }


        private void SaveData()
        {
            //var mstr = (DHR_ClockH.DHRDataWarehouseDataSet.ClockH_MasterRow)MasterTable_Get().Where(n => n.ConfigName == this.cmbConfigName.Text);
            //var mstr = MasterTable_Get().Where(n => n.ConfigName == this.cmbConfigName.Text).First();
            var changes = MasterRow_GetChanges();
            if (changes != null) currentMasterTableAdapter.Update(changes);

            this.currentSettingsTableAdapter.Update(this.dHRDataWarehouseDataSet.ClockH_Settings);
        }

        private void SaveConfig_Click(object sender, EventArgs e)
        {
            this.dgViewMap.EndEdit();

            if (this.txtClientId.Text.Trim() == string.Empty)
            {
                MessageBox.Show("You must supply a Client ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); 
                return;
            }

            var c = this.dHRDataWarehouseDataSet.ClockH_Settings
                           .Where(row => row.ConfigName == this.CurrentMasterRow.ConfigName)
                           .GroupBy(x => x.ColumnIndex)
                            //.Any(G => G.Count() > 1);
                           .Where(G => G.Count() > 1);

            if (c.Count() > 0)
            {
                MessageBox.Show("You have duplicate values in the 'ColumnIndex' column.\nPlease correct the issues and try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.SaveData();
        }

        private void newConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmInputName testDialog = new frmInputName();

            // Show testDialog as a modal dialog and determine if DialogResult = OK.
            if (testDialog.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
                this.CurrentMasterRow = this.dHRDataWarehouseDataSet.ClockH_Master.AddClockH_MasterRow(testDialog.txtDescription.Text, "", "CSV", this.chkFileHasHeader.Checked);
                this.currentMasterTableAdapter.Update(this.CurrentMasterRow);

                this.SavedConfigs_Load();
                //this.cmbConfigName.SelectedValue = testDialog.txtDescription.Text;
                this.cmbConfigName.SelectedIndex = this.cmbConfigName.FindStringExact(testDialog.txtDescription.Text);
            }
            
            testDialog.Dispose();
        }

 

        void ClockH_Settings_TableNewRow(object sender, DataTableNewRowEventArgs e)
        {
            (e.Row as DHR_ClockH.DHRDataWarehouseDataSet.ClockH_SettingsRow).ConfigName = this.cmbConfigName.Text;
        }


        private void cmbConfigName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.HasDataChanged())
            {
                if (MessageBox.Show("Some of the data has changed for the selected configuration\nDo you want to save the changes?", 
                    "",MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                {
                    this.SaveData();
                }
            }

            this.CurrentMasterRow_Get(((ComboBox)sender).Text);
        }

        private void dgViewMap_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        /// <summary>
        /// Processes the csv file into a generic batch/check entry
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btProcess_Click(object sender, EventArgs e)
        {
            var csv = new csvProcessor();

            var ds = csv.ImportFile(this.FileName, this.chkFileHasHeader.Checked);
            var d = this.clockHSettingsBindingSource.DataSource;
            
            //Get the employee columns
            var ees = (from c in (this.clockHSettingsBindingSource.DataSource as DHR_ClockH.DHRDataWarehouseDataSet).ClockH_Settings
                        where c.ColumnType == 3 && c.ConfigName == this.CurrentMasterRow.ConfigName
                        select c);


            //Get the rate columns
            var rates = (from c in (this.clockHSettingsBindingSource.DataSource as DHR_ClockH.DHRDataWarehouseDataSet).ClockH_Settings
                        where c.ColumnType == 4 && c.ConfigName == this.CurrentMasterRow.ConfigName
                        select c);

            //Get the paycode columns
            var payCodeColsList = (from c in (this.clockHSettingsBindingSource.DataSource as DHR_ClockH.DHRDataWarehouseDataSet).ClockH_Settings
                        where   c.ColumnType == 1 
                            &&  c.ConfigName == this.CurrentMasterRow.ConfigName
                            &&  c.PayCode != null
                        select c)
                        //Where(p => p.PayCode != null)
                        ;

            if (ees.Count() == 0)
            {
                var msg = "There is no Employee column defined in your config settings;";
                msg += "\nPlease correct the issues and try again.";

                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (rates.Count() == 0)
            {
                var msg = "There is no Rate column defined in your config settings;";
                msg += "\nPlease correct the issues and try again.";

                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (payCodeColsList.Count() == 0)
            {
                var msg = "There are no paycodes defined in your config settings;";
                msg += "\nyou must enter at least on pay code column.";
                msg += "\nPlease correct the issues and try again.";

                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var eeCol = ees.First();
            var rateCol = rates.First();

            //Create the dataset with all the unpivoted data
            var clientPaySheet = new DHRDataWarehouseDataSet.PaySheetDataTable();
            foreach (System.Data.DataRow clientPaysheetRow in ds.Tables[0].Rows)
            {
                foreach (DHRDataWarehouseDataSet.ClockH_SettingsRow payColItem in payCodeColsList)
                {
                    var row = (DHRDataWarehouseDataSet.PaySheetRow)clientPaySheet.NewRow();
                    row.EE = clientPaysheetRow.ItemArray[eeCol.ColumnIndex - 1].ToString();
                    row.Rate = clientPaysheetRow.ItemArray[rateCol.ColumnIndex - 1].ToString();

                    row.Hours = clientPaysheetRow.ItemArray[payColItem.ColumnIndex - 1].ToString();
                    row.PayCode_Customer = payColItem.ColumnName;
                    row.Paycode_Darwin= payColItem.PayCode;

                    clientPaySheet.Rows.Add(row);
                }
            }

            //finally write the file to disk
            var fileNameOut = Path.Combine(Path.GetDirectoryName(this.FileName),
                                Path.GetFileNameWithoutExtension(this.FileName) + "_out.csv");

            csv.Export2File(clientPaySheet, fileNameOut, true);

            MessageBox.Show("File successfully created", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (this.HasDataChanged())
            {
                if (MessageBox.Show("Some of the data has changed for the selected configuration.\nDo you want to exit the program and lose the changes?",
                    "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }


            if (this.currentMasterTableAdapter.Connection.State != ConnectionState.Closed)
                this.currentMasterTableAdapter.Connection.Close();

            if (this.currentSettingsTableAdapter.Connection.State != ConnectionState.Closed)
                this.currentSettingsTableAdapter.Connection.Close();

            if (this.clockH_MasterTableAdapter.Connection.State != ConnectionState.Closed)
                this.clockH_MasterTableAdapter.Connection.Close();

            
            //System.Windows.Forms.Application.Exit();
        }

        private void deleteThisConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (MessageBox.Show("Are you sure you want to delete this config?\nDeleting a config will remove all related entries in the\ndatabase; this action can not be undone.", "Please confirm",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {

                this.currentSettingsTableAdapter.DeleteConfigSettings(this.CurrentMasterRow.ConfigName);
                this.currentMasterTableAdapter.DeleteConfig(this.CurrentMasterRow.ConfigName);

                Form1_Load(null, null);
            }
        }
    }
}
