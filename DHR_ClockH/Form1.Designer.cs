﻿namespace DHR_ClockH
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btSelectFileToMap = new System.Windows.Forms.Button();
            this.ofdSelectFileToMap = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.dgViewMap = new System.Windows.Forms.DataGridView();
            this.ConfigName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clockHSettingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dHRDataWarehouseDataSet = new DHR_ClockH.DHRDataWarehouseDataSet();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chkFileHasHeader = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btProcess = new System.Windows.Forms.Button();
            this.cmbConfigName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtClientId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbFormat = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fILEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteThisConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.clockH_MasterTableAdapter = new DHR_ClockH.DHRDataWarehouseDataSetTableAdapters.ClockH_MasterTableAdapter();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgViewMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clockHSettingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dHRDataWarehouseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btSelectFileToMap
            // 
            this.btSelectFileToMap.Location = new System.Drawing.Point(102, 21);
            this.btSelectFileToMap.Name = "btSelectFileToMap";
            this.btSelectFileToMap.Size = new System.Drawing.Size(45, 22);
            this.btSelectFileToMap.TabIndex = 0;
            this.btSelectFileToMap.Text = "...";
            this.btSelectFileToMap.UseVisualStyleBackColor = true;
            this.btSelectFileToMap.Click += new System.EventHandler(this.btSelectFileToMap_Click);
            // 
            // ofdSelectFileToMap
            // 
            this.ofdSelectFileToMap.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select File:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dgViewMap
            // 
            this.dgViewMap.AutoGenerateColumns = false;
            this.dgViewMap.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgViewMap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgViewMap.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ConfigName,
            this.ColumnType,
            this.ColumnIndex,
            this.ColumnName,
            this.PayCode});
            this.dgViewMap.DataSource = this.clockHSettingsBindingSource;
            this.dgViewMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgViewMap.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgViewMap.Location = new System.Drawing.Point(0, 0);
            this.dgViewMap.Name = "dgViewMap";
            this.dgViewMap.Size = new System.Drawing.Size(623, 310);
            this.dgViewMap.TabIndex = 2;
            this.dgViewMap.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgViewMap_DataError);
            // 
            // ConfigName
            // 
            this.ConfigName.DataPropertyName = "ConfigName";
            this.ConfigName.HeaderText = "ConfigName";
            this.ConfigName.Name = "ConfigName";
            this.ConfigName.ReadOnly = true;
            // 
            // ColumnType
            // 
            this.ColumnType.DataPropertyName = "ColumnType";
            this.ColumnType.HeaderText = "ColumnType";
            this.ColumnType.Name = "ColumnType";
            this.ColumnType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ColumnIndex
            // 
            this.ColumnIndex.DataPropertyName = "ColumnIndex";
            this.ColumnIndex.HeaderText = "ColumnIndex";
            this.ColumnIndex.Name = "ColumnIndex";
            // 
            // ColumnName
            // 
            this.ColumnName.DataPropertyName = "ColumnName";
            this.ColumnName.HeaderText = "ColumnName";
            this.ColumnName.Name = "ColumnName";
            // 
            // PayCode
            // 
            this.PayCode.DataPropertyName = "PayCode";
            this.PayCode.HeaderText = "PayCode";
            this.PayCode.Name = "PayCode";
            // 
            // clockHSettingsBindingSource
            // 
            this.clockHSettingsBindingSource.DataMember = "ClockH_Settings";
            this.clockHSettingsBindingSource.DataSource = this.dHRDataWarehouseDataSet;
            // 
            // dHRDataWarehouseDataSet
            // 
            this.dHRDataWarehouseDataSet.DataSetName = "DHRDataWarehouseDataSet";
            this.dHRDataWarehouseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chkFileHasHeader);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            this.splitContainer1.Panel1.Controls.Add(this.cmbConfigName);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.txtClientId);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.cmbFormat);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgViewMap);
            this.splitContainer1.Size = new System.Drawing.Size(623, 461);
            this.splitContainer1.SplitterDistance = 147;
            this.splitContainer1.TabIndex = 3;
            // 
            // chkFileHasHeader
            // 
            this.chkFileHasHeader.AutoSize = true;
            this.chkFileHasHeader.Location = new System.Drawing.Point(249, 26);
            this.chkFileHasHeader.Name = "chkFileHasHeader";
            this.chkFileHasHeader.Size = new System.Drawing.Size(105, 17);
            this.chkFileHasHeader.TabIndex = 12;
            this.chkFileHasHeader.Text = "Contains Header";
            this.chkFileHasHeader.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btProcess);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btSelectFileToMap);
            this.panel1.Location = new System.Drawing.Point(411, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(172, 106);
            this.panel1.TabIndex = 11;
            // 
            // btProcess
            // 
            this.btProcess.Location = new System.Drawing.Point(26, 59);
            this.btProcess.Name = "btProcess";
            this.btProcess.Size = new System.Drawing.Size(121, 29);
            this.btProcess.TabIndex = 8;
            this.btProcess.Text = "Process";
            this.btProcess.UseVisualStyleBackColor = true;
            this.btProcess.Click += new System.EventHandler(this.btProcess_Click);
            // 
            // cmbConfigName
            // 
            this.cmbConfigName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConfigName.FormattingEnabled = true;
            this.cmbConfigName.Location = new System.Drawing.Point(85, 24);
            this.cmbConfigName.Name = "cmbConfigName";
            this.cmbConfigName.Size = new System.Drawing.Size(121, 21);
            this.cmbConfigName.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Name:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtClientId
            // 
            this.txtClientId.Location = new System.Drawing.Point(86, 62);
            this.txtClientId.Name = "txtClientId";
            this.txtClientId.Size = new System.Drawing.Size(120, 20);
            this.txtClientId.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Format:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cmbFormat
            // 
            this.cmbFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFormat.FormattingEnabled = true;
            this.cmbFormat.Items.AddRange(new object[] {
            "CSV",
            "Fixed Length"});
            this.cmbFormat.Location = new System.Drawing.Point(86, 101);
            this.cmbFormat.Name = "cmbFormat";
            this.cmbFormat.Size = new System.Drawing.Size(120, 21);
            this.cmbFormat.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Client ID:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fILEToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(623, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fILEToolStripMenuItem
            // 
            this.fILEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newConfigToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.deleteThisConfigToolStripMenuItem});
            this.fILEToolStripMenuItem.Name = "fILEToolStripMenuItem";
            this.fILEToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fILEToolStripMenuItem.Text = "File";
            // 
            // newConfigToolStripMenuItem
            // 
            this.newConfigToolStripMenuItem.Name = "newConfigToolStripMenuItem";
            this.newConfigToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.newConfigToolStripMenuItem.Text = "New Config";
            this.newConfigToolStripMenuItem.Click += new System.EventHandler(this.newConfigToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveConfig_Click);
            // 
            // deleteThisConfigToolStripMenuItem
            // 
            this.deleteThisConfigToolStripMenuItem.Name = "deleteThisConfigToolStripMenuItem";
            this.deleteThisConfigToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.deleteThisConfigToolStripMenuItem.Text = "Delete this config";
            this.deleteThisConfigToolStripMenuItem.Click += new System.EventHandler(this.deleteThisConfigToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 485);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.ShowItemToolTips = true;
            this.statusStrip1.Size = new System.Drawing.Size(623, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(702, 17);
            this.lblStatus.Spring = true;
            this.lblStatus.Text = "No file selected...";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // clockH_MasterTableAdapter
            // 
            this.clockH_MasterTableAdapter.ClearBeforeFill = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(623, 461);
            this.panel2.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 507);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Timesheet Config";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgViewMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clockHSettingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dHRDataWarehouseDataSet)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btSelectFileToMap;
        private System.Windows.Forms.OpenFileDialog ofdSelectFileToMap;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgViewMap;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox txtClientId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbFormat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btProcess;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fILEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newConfigToolStripMenuItem;
        private System.Windows.Forms.ComboBox cmbConfigName;
        private DHRDataWarehouseDataSet dHRDataWarehouseDataSet;
        private DHRDataWarehouseDataSetTableAdapters.ClockH_MasterTableAdapter clockH_MasterTableAdapter;
        private System.Windows.Forms.BindingSource clockHSettingsBindingSource;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConfigName;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayCode;
        private System.Windows.Forms.CheckBox chkFileHasHeader;
        private System.Windows.Forms.ToolStripMenuItem deleteThisConfigToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
    }
}

