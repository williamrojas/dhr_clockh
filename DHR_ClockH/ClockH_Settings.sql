--drop table ClockH_Settings

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.ClockH_Settings
	(
	Id			int				IDENTITY(1,1) PRIMARY KEY,
	ConfigName	nvarchar(50)	FOREIGN KEY REFERENCES ClockH_Master(ConfigName), 		
	ColumnType	smallint 		FOREIGN KEY REFERENCES ClockH_ColumnTypes(Id), 		
	ColumnName	nvarchar(10) 	NOT NULL,
	ColumnIndex	smallint 		NOT NULL,
	PayCode		nvarchar(10) 	NULL
	)  ON [PRIMARY]
GO
--ALTER TABLE dbo.Table_1 SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/*


INSERT INTO [dbo].[ClockH_Settings]     VALUES         ('Test1', 1, 'ColName1', 0, 'PayCode1')
INSERT INTO [dbo].[ClockH_Settings]     VALUES         ('Test1', 2, 'ColName2', 0, 'PayCode2')
INSERT INTO [dbo].[ClockH_Settings]     VALUES         ('Test1', 3, 'ColNamn3', 0, 'PayCode3')
INSERT INTO [dbo].[ClockH_Settings]     VALUES         ('Test1', 4, 'ColName4', 0, 'PayCode4')
																				
INSERT INTO [dbo].[ClockH_Settings]     VALUES         ('Test2', 1, 'ColName1', 0, 'PayCode1')
INSERT INTO [dbo].[ClockH_Settings]     VALUES         ('Test2', 2, 'ColName2', 0, 'PayCode2')
INSERT INTO [dbo].[ClockH_Settings]     VALUES         ('Test2', 3, 'ColName3', 0, 'PayCode3')
INSERT INTO [dbo].[ClockH_Settings]     VALUES         ('Test2', 4, 'ColName4', 0, 'PayCode4')

GO
*/