﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR_ClockH
{
    class csvProcessor
    {
        public DataSet ImportFile2(string csvfileName)
        {
            /* First read the excel file into a dataset */
            OleDbConnection conn = new OleDbConnection(("provider=Microsoft.Jet.OLEDB.4.0; " +
                //("data source=" + this.settings.YTDCodesMapFile +
                //("data source=" + csvfileName +
                                                        ("data source=" + Path.GetDirectoryName(csvfileName) +
                //"; Extended Properties='Excel 8.0;IMEX=1';")));           // excel
                //"; Extended Properties='Excel 8.0;IMEX=1;HDR=YES;FMT=Delimited';")));           // excel
                //"; Extended Properties='Text;HDR=YES;FMT=Delimited';")));   // csv
                                                        "; Extended Properties='Text;HDR=No;FMT=Delimited'")));   // csv

            // Select the data from Sheet1 of the workbook.
            //OleDbDataAdapter ada = new OleDbDataAdapter("select * from [map$]", conn);
            //OleDbDataAdapter ada = new OleDbDataAdapter(string.Format("select * from [{0}]", Path.GetFileNameWithoutExtension(csvfileName)), conn);
            OleDbDataAdapter ada = new OleDbDataAdapter(string.Format("select * from [{0}]", Path.GetFileName(csvfileName)), conn);
            DataSet ds = new DataSet();

            //DataTable dt = new DataTable("YTDCodes");
            //dt.Columns.Add(new DataColumn("PayCodeID_Legacy", typeof(string)));
            //dt.Columns.Add(new DataColumn("PayCodeID_Darwin", typeof(string)));
            //dt.Columns.Add(new DataColumn("Description", typeof(string)));
            //ada.Fill(dt);
            //ds.Tables.Add(dt);
            ada.Fill(ds);
            conn.Close();

            return ds;
        }

        public List<string> GetColumnNamesFromCsv(string fileName)
        {
            using (var rd = new StreamReader(fileName))
            {
                return rd.ReadLine().Split(',').ToList();
            }
        }


        /// <summary>
        /// Imports a csv dfile into a dataset
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="hasHeader"></param>
        /// <returns></returns>
        public DataSet ImportFile(string fileName, bool hasHeader)
        {
            var dt = new DataTable();
            var rowNumber = 0;
            using (var rd = new StreamReader(fileName))
            {
                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (rowNumber == 0) //header   
                    {
                        if (hasHeader)
                        {
                            foreach (var item in splits)
                                dt.Columns.Add(item);
                        }
                        else
                        {
                            for (int i = 0; i < splits.Count(); i++)
                                dt.Columns.Add("col" + i.ToString());

                            dt.Rows.Add(splits);
                        }
                    }
                    else
                    {
                        dt.Rows.Add(splits);
                    }

                    rowNumber++;
                }
            }


            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            return ds;
        }


        public void Export2File(DataTable table, string fileNameFullPath, bool writeHeader)
        {
            using (var fileStream = new FileStream(fileNameFullPath, FileMode.OpenOrCreate))
            {
                using (var streamWriter = new StreamWriter(fileStream))
                {
                    if (writeHeader)
                        streamWriter.WriteLine(string.Join(",", table.Columns.Cast<DataColumn>().Select(column => column.ColumnName)));

                    foreach (DataRow row in table.Rows)
                    {
                        IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                        streamWriter.WriteLine(string.Join(",", fields));
                    }
                }
            }
        }
    }
}
