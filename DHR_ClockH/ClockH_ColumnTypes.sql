BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.ClockH_ColumnTypes
	(
	Id smallint					IDENTITY(1,1) PRIMARY KEY,
	Description nvarchar(50)	NOT NULL,
	)
GO
--ALTER TABLE dbo.Table_1 SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

--drop table ClockH_ColumnTypes
select * from ClockH_ColumnTypes

insert into ClockH_ColumnTypes (Description)	values ('Pay Item');
insert into ClockH_ColumnTypes (Description)	values ('Pay Item');
insert into ClockH_ColumnTypes (Description)	values ('Job Cost');
insert into ClockH_ColumnTypes (Description)	values ('Employee Id');
insert into ClockH_ColumnTypes (Description)	values ('Pay Rate');
insert into ClockH_ColumnTypes (Description)	values ('Dollar Amount');
insert into ClockH_ColumnTypes (Description)	values ('Comp Code');
insert into ClockH_ColumnTypes (Description)	values ('Job Segment 1');
insert into ClockH_ColumnTypes (Description)	values ('Job Segment 2');
insert into ClockH_ColumnTypes (Description)	values ('Job Segment 3');
insert into ClockH_ColumnTypes (Description)	values ('Division');
insert into ClockH_ColumnTypes (Description)	values ('Pay Code for Item');
insert into ClockH_ColumnTypes (Description)	values ('Job Segment 4');
insert into ClockH_ColumnTypes (Description)	values ('Pay Code Lookup');
insert into ClockH_ColumnTypes (Description)	values ('Pay Item Always Hours');
insert into ClockH_ColumnTypes (Description)	values ('Pay Rate Lookup');
insert into ClockH_ColumnTypes (Description)	values ('Shift Group');
insert into ClockH_ColumnTypes (Description)	values ('Temp Processing Group');
insert into ClockH_ColumnTypes (Description)	values ('Deduction Code');
insert into ClockH_ColumnTypes (Description)	values ('Deduction Plan');
insert into ClockH_ColumnTypes (Description)	values ('Employee Dedcution Amount');
insert into ClockH_ColumnTypes (Description)	values ('Employer Dedcution Amount');
insert into ClockH_ColumnTypes (Description)	values ('Certified Date Worked');
insert into ClockH_ColumnTypes (Description)	values ('Special - reverse rate/amount');
insert into ClockH_ColumnTypes (Description)	values ('Dollar Amount for Hours');
insert into ClockH_ColumnTypes (Description)	values ('Employee Deduction Amount no Override');